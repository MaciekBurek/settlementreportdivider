package com.cuma.omnipack;

import org.apache.poi.ss.usermodel.Row;

import static com.cuma.omnipack.Constants.*;

/**
 * Created by Maciek on 19.09.2018.
 */
public class DecodingRowFiller implements RowFiller {
    private Row row;

    @Override
    public void fillRow(Entry entry, Row row) {
        this.row = row;

        createCell(COURIER_COLUMN_NUMBER, entry.getCourier());
        createCell(LETTER_NUMBER_COLUMN_NUMBER, entry.getLetterNumber());
        createCell(AMOUNT_COLUMN_NUMBER, entry.getAmount());
        createCell(CLIENT_COLUMN_NUMBER, entry.getClient());
        createCell(ORDER_COLUMN_NUMBER, entry.getOrderNumber());
    }

    private void createCell(int columnNumber, String value) {
        row.createCell(columnNumber).setCellValue(value);
    }

    private void createCell(int columnNumber, double value) {
        row.createCell(columnNumber).setCellValue(value);
    }
}
