package com.cuma.omnipack;

import org.apache.poi.ss.usermodel.Sheet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Maciek on 19.09.2018.
 */
public class XlsxSheet implements EntriesCollection {
    private List<Entry> entries = new ArrayList<>();

    public XlsxSheet(Sheet sheet, RowConverter converter) {
        for (int i = 1; i < sheet.getPhysicalNumberOfRows(); i++) {
            try {
                addEntry(converter.convert(sheet.getRow(i)));
            } catch (IncorrectRowException e) {
                //todo: what in that case?
            }
        }
    }

    public XlsxSheet(Sheet sheet) {
        this(sheet, new DecodingRowConverter());
    }

    public XlsxSheet() {

    }

    public XlsxSheet(Entry entry) {
        addEntry(entry);
    }

    public void addEntry(Entry entry) {
        entries.add(entry);
    }

    public List<Entry> getEntries() {
        return entries;
    }
}
