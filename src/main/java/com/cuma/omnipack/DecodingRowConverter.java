package com.cuma.omnipack;

import org.apache.poi.ss.usermodel.Row;

import static com.cuma.omnipack.Constants.*;

/**
 * * Created by Maciek on 19.09.2018.
 */
public class DecodingRowConverter implements RowConverter {
    private final Validator<Row> validator;

    public DecodingRowConverter(Validator<Row> validator) {
        this.validator = validator;
    }

    public DecodingRowConverter() {
        this(new RowValidator());
    }

    @Override
    public DecodedEntry convert(Row row) throws IncorrectRowException {
        if(!validator.isCorrect(row)) {
            throw new IncorrectRowException();
        }

        return new DecodedEntry(row.getCell(COURIER_COLUMN_NUMBER).getStringCellValue(),
                row.getCell(LETTER_NUMBER_COLUMN_NUMBER).getStringCellValue(),
                row.getCell(AMOUNT_COLUMN_NUMBER).getNumericCellValue(),
                row.getCell(CLIENT_COLUMN_NUMBER).getStringCellValue(),
                row.getCell(ORDER_COLUMN_NUMBER).getStringCellValue());
    }
}
