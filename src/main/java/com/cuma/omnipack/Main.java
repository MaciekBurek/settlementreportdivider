package com.cuma.omnipack;

import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * Created by Maciek on 19.09.2018.
 */
public class Main {
    public static void main(String[] args) {
        if(args.length != 1) {
            System.exit(1);
        }

        try {
            Workbook workbook = WorkbookFactory.create(new File(args[0]));
            XlsxSheet xlsxSheet = new XlsxSheet(workbook.getSheetAt(0));
            Map<String, EntriesCollection> outputXlsxSheets = new SheetDivider().divide(xlsxSheet);

            OutputSheetCreator creator = new OutputSheetCreator(new DecodingRowFiller());

            for (Map.Entry<String, EntriesCollection> mapEntry : outputXlsxSheets.entrySet()) {
                creator.saveSheet(new FileOutputStream(getFileName(mapEntry.getKey())), mapEntry.getValue());
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }
    }

    private static String getFileName(String key) {
        String date = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"));
        return key + "_report_" + date + ".xlsx";
    }
}
