package com.cuma.omnipack;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Maciek on 19.09.2018.
 */
public class SheetDivider {
    public Map<String, EntriesCollection> divide(EntriesCollection sheet) {
        Map<String, EntriesCollection> map = new HashMap<>();

        for (Entry entry : sheet.getEntries()) {
            if (map.containsKey(entry.getClient())) {
                map.get(entry.getClient()).addEntry(entry);
            } else {
                map.put(entry.getClient(), new XlsxSheet(entry));
            }
        }

        return map;
    }
}
