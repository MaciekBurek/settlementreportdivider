package com.cuma.omnipack;

/**
 * @author Maciej Burek (maciej.burek@accenture.com)
 */
public interface Validator<T> {
    boolean isCorrect(T t);
}
