package com.cuma.omnipack;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Created by Maciek on 19.09.2018.
 */
public class OutputSheetCreator {
    private final DecodingRowFiller rowFiller;
    private Sheet sheet;

    public OutputSheetCreator(DecodingRowFiller rowFiller) {
        this.rowFiller = rowFiller;
    }

    void saveSheet(OutputStream out, EntriesCollection xlsxSheet) throws IOException {
        Workbook workbook = new HSSFWorkbook();
        sheet = workbook.createSheet();
        List<Entry> entries = xlsxSheet.getEntries();

        setHeader();

        for (int i = 0; i < entries.size(); i++) {
            Row row = sheet.createRow(i + 1);
            rowFiller.fillRow(entries.get(i), row);
        }

        setFooter(entries.size());

        workbook.write(out);
        workbook.close();
    }

    private void setHeader() {
        Row row = sheet.createRow(0);
        row.createCell(Constants.COURIER_COLUMN_NUMBER).setCellValue("kurier");
        row.createCell(Constants.LETTER_NUMBER_COLUMN_NUMBER).setCellValue("Nr listu");
        row.createCell(Constants.AMOUNT_COLUMN_NUMBER).setCellValue("kwota");
        row.createCell(Constants.CLIENT_COLUMN_NUMBER).setCellValue("klient");
        row.createCell(Constants.ORDER_COLUMN_NUMBER).setCellValue("Nr Zam");
    }

    private void setFooter(int lastRowNumber) {
        sheet.createRow(sheet.getPhysicalNumberOfRows());
        Row row = sheet.createRow(sheet.getPhysicalNumberOfRows());

        row.createCell(Constants.AMOUNT_COLUMN_NUMBER - 1).setCellValue("Suma:");
        row.createCell(Constants.AMOUNT_COLUMN_NUMBER).setCellFormula("SUM(C2:C" + lastRowNumber + ")");
    }
}
