package com.cuma.omnipack;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;

/**
 * @author Maciej Burek (maciej.burek@accenture.com)
 */
public class RowValidator implements Validator<Row> {
    @Override
    public boolean isCorrect(Row row) {
        return row.getPhysicalNumberOfCells() == 5 &&
                row.getCell(Constants.AMOUNT_COLUMN_NUMBER).getCellType() == CellType.NUMERIC;
    }
}
