package com.cuma.omnipack;

/**
 * * Created by Maciek on 19.09.2018.
 */
public class Constants {
    private Constants() {
    }

    public static final int COURIER_COLUMN_NUMBER = 0;
    public static final int LETTER_NUMBER_COLUMN_NUMBER = 1;
    public static final int AMOUNT_COLUMN_NUMBER = 2;
    public static final int CLIENT_COLUMN_NUMBER = 3;
    public static final int ORDER_COLUMN_NUMBER = 4;
}
