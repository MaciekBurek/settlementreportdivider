package com.cuma.omnipack;

/**
 * Created by Maciek on 19.09.2018.
 */
public class DecodedEntry implements Entry {
    private final String courier;
    private final String letterNumber;
    private final double amount;
    private final String client;
    private final String orderNumber;

    @Override
    public String getClient() {
        return client;
    }

    @Override
    public String getCourier() {
        return courier;
    }

    @Override
    public String getLetterNumber() {
        return letterNumber;
    }

    @Override
    public double getAmount() {
        return amount;
    }

    @Override
    public String getOrderNumber() {
        return orderNumber;
    }

    public DecodedEntry(String courier, String letterNumber, double amount, String client, String orderNumber) {
        this.courier = courier;
        this.letterNumber = letterNumber;
        this.amount = amount;
        this.client = client;
        this.orderNumber = orderNumber;
    }
}
