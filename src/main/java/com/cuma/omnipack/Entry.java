package com.cuma.omnipack;

/**
 * Created by Maciek on 19.09.2018.
 */
public interface Entry {
    String getClient();

    String getCourier();

    String getLetterNumber();

    double getAmount();

    String getOrderNumber();
}
