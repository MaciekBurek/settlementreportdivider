package com.cuma.omnipack;

import org.apache.poi.ss.usermodel.Row;

/**
 * Created by Maciek on 19.09.2018.
 */
public interface RowFiller {
    void fillRow(Entry entry, Row row);
}
