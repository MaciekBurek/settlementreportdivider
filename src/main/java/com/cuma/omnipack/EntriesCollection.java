package com.cuma.omnipack;

import java.util.List;

/**
 * Created by Maciek on 19.09.2018.
 */
public interface EntriesCollection {
    void addEntry(Entry entry);

    List<Entry> getEntries();
}
