package com.cuma.omnipack

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

/**
 * Created by Maciek on 19.09.2018.
 */
class DecodingRowFillerTest extends Specification {
    void setRowTest() {
        given: "test values"
        String courier = "courier"
        String letterNumber = "letterNumber"
        double amount = 0
        String client = "client"
        String orderNumber = "orderNumber"

        and: "entry with these values"
        Entry entry = Stub(Entry)
        entry.getCourier() >> courier
        entry.getLetterNumber() >> letterNumber
        entry.getAmount() >> amount
        entry.getClient() >> client
        entry.getOrderNumber() >> orderNumber

        and: "cells to be filled"
        Cell courierCell = Mock(Cell)
        Cell letterNumberCell = Mock(Cell)
        Cell amountCell = Mock(Cell)
        Cell clientCell = Mock(Cell)
        Cell orderNumberCell = Mock(Cell)

        and: "row to be filled with these cells"
        Row row = Mock(Row)

        when: "filler fills the row"
        DecodingRowFiller filler = new DecodingRowFiller()
        filler.fillRow(entry, row)

        then: "all cells are created"
        1 * row.createCell(Constants.COURIER_COLUMN_NUMBER) >> courierCell
        1 * row.createCell(Constants.LETTER_NUMBER_COLUMN_NUMBER) >> letterNumberCell
        1 * row.createCell(Constants.AMOUNT_COLUMN_NUMBER) >> amountCell
        1 * row.createCell(Constants.CLIENT_COLUMN_NUMBER) >> clientCell
        1 * row.createCell(Constants.ORDER_COLUMN_NUMBER) >> orderNumberCell

        and: "filled with values"
        1 * courierCell.setCellValue(courier)
        1 * letterNumberCell.setCellValue(letterNumber)
        1 * amountCell.setCellValue(amount)
        1 * clientCell.setCellValue(client)
        1 * orderNumberCell.setCellValue(orderNumber)
    }
}
