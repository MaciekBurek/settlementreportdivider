package com.cuma.omnipack

import spock.lang.Specification

/**
 * Created by Maciek on 19.09.2018.
 */
class SheetDividerTest extends Specification {
    private SheetDivider divider = new SheetDivider()

    void emptySheetDividedIntoEmptyMap() {
        given: "empty sheet"
        XlsxSheet sheet = new XlsxSheet()

        when: "divider divides that sheet"
        Map<String, EntriesCollection> sheetMap = divider.divide(sheet)

        then: "it returns empty map"
        sheetMap.isEmpty()
    }

    void sheetWithOneEntryDividedIntoOneSheetWithThisClient() {
        given: "one entry"
        String client = "client"
        Entry entry1 = Stub(Entry)
        entry1.getClient() >> client

        and: "sheet with this entry"
        EntriesCollection sheet = Mock(EntriesCollection)
        sheet.getEntries() >> [entry1]

        when: "divider divides that sheet"
        Map<String, EntriesCollection> sheetMap = divider.divide(sheet)

        then: "it returns one element map with the entry"
        sheetMap.size() == 1
        EntriesCollection value = sheetMap.get(client)
        value.getEntries().size() == 1
        value.getEntries().get(0) == entry1
    }

    void sheetWithOneClientDividedIntoOneSheetWithThisClient() {
        given: "entries with same client"
        String client = "client"
        Entry entry1 = Stub(Entry)
        entry1.getClient() >> client
        Entry entry2 = Stub(Entry)
        entry2.getClient() >> client

        and: "sheet with these entries"
        EntriesCollection sheet = Mock(EntriesCollection)
        sheet.getEntries() >> [entry1, entry2]

        when: "divider divides that sheet"
        Map<String, EntriesCollection> sheetMap = divider.divide(sheet)

        then: "it returns one element map with these entries"
        sheetMap.size() == 1
        EntriesCollection value = sheetMap.get(client)
        value.getEntries().size() == 2
        value.getEntries().get(0) == entry1
        value.getEntries().get(1) == entry2
    }

    void sheetWithTwoEntriesWithDifferentClientsDividedIntoTwoSheetsWithTheseClients() {
        given: "entries with different clients"
        String client1 = "client1"
        String client2 = "client2"
        Entry entry1 = Stub(Entry)
        entry1.getClient() >> client1
        Entry entry2 = Stub(Entry)
        entry2.getClient() >> client2

        and: "sheet with these entries"
        EntriesCollection sheet = Mock(EntriesCollection)
        sheet.getEntries() >> [entry1, entry2]

        when: "divider divides that sheet"
        Map<String, EntriesCollection> sheetMap = divider.divide(sheet)

        then: "it returns two elements map with these entries"
        sheetMap.size() == 2

        EntriesCollection value1 = sheetMap.get(client1)
        value1.getEntries().size() == 1
        value1.getEntries().get(0) == entry1

        EntriesCollection value2 = sheetMap.get(client2)
        value2.getEntries().size() == 1
        value2.getEntries().get(0) == entry2
    }
}
