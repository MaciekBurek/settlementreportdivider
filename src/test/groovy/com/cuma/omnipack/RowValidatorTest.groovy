package com.cuma.omnipack

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

/**
 * @author Maciej Burek (maciej.burek@accenture.com)
 */
class RowValidatorTest extends Specification {
    private RowValidator validator = new RowValidator()

    private final Row row = Stub(Row)

    void validateValidRow() {
        given: "numeric cell"
        Cell cell = Stub(Cell)
        cell.getCellType() >> CellType.NUMERIC

        and: "row with 5 cells returning this cell as amount"
        row.getPhysicalNumberOfCells() >> 5
        row.getCell(Constants.AMOUNT_COLUMN_NUMBER) >> cell

        when: "validating the row with the validator"
        boolean isCorrect = validator.isCorrect(row)

        then: "it returns true"
        isCorrect
    }

    void validateRowWithTooFewCells() {
        given: "row with too few cells"
        row.getPhysicalNumberOfCells() >> 4

        when: "validating the row with the validator"
        boolean isCorrect = validator.isCorrect(row)

        then: "it returns false"
        !isCorrect
    }

    void validateRowWithNonNumericAmountCell() {
        given: "non-numeric cell"
        Cell cell = Stub(Cell)
        cell.getCellType() >> CellType.STRING

        and: "row with 5 cells returning this cell as amount"
        row.getPhysicalNumberOfCells() >> 5
        row.getCell(Constants.AMOUNT_COLUMN_NUMBER) >> cell

        when: "validating the row with the validator"
        boolean isCorrect = validator.isCorrect(row)

        then: "it returns false"
        !isCorrect
    }
}
