package com.cuma.omnipack

import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.usermodel.Sheet
import spock.lang.Specification

/**
 * Created by Maciek on 19.09.2018.
 */
class XlsxSheetTest extends Specification {
    private XlsxSheet xlsxSheet

    private final Sheet sheet = Stub(Sheet)
    private final RowConverter rowConverter = Stub(RowConverter)
    private final Row row1 = Stub(Row)
    private final Row row2 = Stub(Row)
    private final Entry entry1 = Stub(Entry)
    private final Entry entry2 = Stub(Entry)

    void newSheet() {
        given: "new sheet"
        xlsxSheet = new XlsxSheet(sheet, rowConverter)

        when: "we get its entries list"
        List<Entry> entries = xlsxSheet.getEntries()

        then: "the size is 0"
        entries.size() == 0
    }

    void addEntries() {
        given: "entries"
        Entry entry1 = Stub(Entry)
        Entry entry2 = Stub(Entry)

        and: "newly created sheet"
        xlsxSheet = new XlsxSheet()

        when: "these entries are added to the sheet"
        xlsxSheet.addEntry(entry1)
        xlsxSheet.addEntry(entry2)

        and: "entries list is taken"
        List<Entry> entries = xlsxSheet.getEntries()

        then: "it's filled with these entries"
        xlsxSheet.getEntries().size() == 2
        xlsxSheet.getEntries().get(0) == entry1
        xlsxSheet.getEntries().get(1) == entry2
    }

    void emptySheet() {
        given: "sheet with no rows"
        sheet.getPhysicalNumberOfRows() >> 0

        when: "new xlsxSheet is created with this sheet"
        xlsxSheet = new XlsxSheet(sheet, rowConverter)

        then: "it returns empty entry list"
        xlsxSheet.getEntries().size() == 0
    }

    void sheetWithHeaderOnly() {
        given: "sheet with only header"
        sheet.getPhysicalNumberOfRows() >> 1

        when: "new xlsxSheet is created with this sheet"
        xlsxSheet = new XlsxSheet(sheet, rowConverter)

        then: "it returns empty entry list"
        xlsxSheet.getEntries().size() == 0
    }

    void sheetWith2Elements() {
        given: "sheet with header and two rows"
        sheet.getPhysicalNumberOfRows() >> 3
        sheet.getRow(1) >> row1
        sheet.getRow(2) >> row2

        and: "row converter converting these rows into entries"
        rowConverter.convert(row1) >> entry1
        rowConverter.convert(row2) >> entry2

        when: "new xlsxSheet is created with these sheet and converter"
        xlsxSheet = new XlsxSheet(sheet, rowConverter)

        then: "it returns two element list with these entries"
        xlsxSheet.getEntries().size() == 2
        xlsxSheet.getEntries().get(0) == entry1
        xlsxSheet.getEntries().get(1) == entry2
    }

    void sheetWithIncorrectRows() {
        given: "sheet with header and two rows"
        sheet.getPhysicalNumberOfRows() >> 3
        sheet.getRow(1) >> row1
        sheet.getRow(2) >> row2

        and: "row converter throwing an incorrect row exception for first one"
        rowConverter.convert(row1) >> { throw new IncorrectRowException() }
        rowConverter.convert(row2) >> entry2

        when: "new xlsxSheet is created with these sheet and converter"
        xlsxSheet = new XlsxSheet(sheet, rowConverter)

        then: "it returns one element list with second entry"
        xlsxSheet.getEntries().size() == 1
        xlsxSheet.getEntries().get(0) == entry2
    }
}
