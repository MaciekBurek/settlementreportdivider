package com.cuma.omnipack

import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.CellType
import org.apache.poi.ss.usermodel.Row
import spock.lang.Specification

/**
 * * Created by Maciek on 19.09.2018.
 */
class DecodingRowConverterTest extends Specification {
    private DecodingRowConverter converter;

    private final Validator<Row> validator = Stub(Validator)
    private final Row row = Stub(Row)

    void convertValidRow() {
        given: "entry values"
        String courier = "courier"
        String letterNumber = "letterNumber"
        double amount = 0
        String client = "client"
        String orderNumber = "orderNumber"

        and: "cells with these values"
        Cell courierCell = Stub(Cell)
        courierCell.getStringCellValue() >> courier

        Cell letterNumberCell = Stub(Cell)
        letterNumberCell.getStringCellValue() >> letterNumber

        Cell amountCell = Stub(Cell)
        amountCell.getNumericCellValue() >> amount
        amountCell.getCellType() >> CellType.NUMERIC

        Cell clientCell = Stub(Cell)
        clientCell.getStringCellValue() >> client

        Cell orderNumberCell = Stub(Cell)
        orderNumberCell.getStringCellValue() >> orderNumber

        and: "row with these cells"
        row.getCell(Constants.COURIER_COLUMN_NUMBER) >> courierCell
        row.getCell(Constants.LETTER_NUMBER_COLUMN_NUMBER) >> letterNumberCell
        row.getCell(Constants.AMOUNT_COLUMN_NUMBER) >> amountCell
        row.getCell(Constants.CLIENT_COLUMN_NUMBER) >> clientCell
        row.getCell(Constants.ORDER_COLUMN_NUMBER) >> orderNumberCell

        and: "validator returning correct"
        validator.isCorrect(row) >> true

        and: "converter with this validator"
        converter = new DecodingRowConverter(validator)

        when: "converting the row with the converter"
        DecodedEntry entry = converter.convert(row)

        then: "we get entry with set values"
        entry.getCourier() == courier
        entry.getLetterNumber() == letterNumber
        entry.getAmount() == amount
        entry.getClient() == client
        entry.getOrderNumber() == orderNumber
    }

    void convertInvalidRow() {
        given: "validator returning incorrect"
        validator.isCorrect(row) >> false

        and: "converter with this validator"
        converter = new DecodingRowConverter(validator)

        when: "converting the row with the converter"
        converter.convert(row)

        then: "incorrect row exception is thrown"
        thrown IncorrectRowException
    }
}
